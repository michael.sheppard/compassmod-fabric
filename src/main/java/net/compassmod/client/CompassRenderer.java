/*
 * CompassRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.compassmod.client;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RotationAxis;
import net.minecraft.util.math.Vec3d;
import net.compassmod.common.CompassMod;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;


@Environment(EnvType.CLIENT)
public class CompassRenderer implements HudRenderCallback {
    private static final Identifier COMPASS_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-compass.png");
    private static final Identifier WAYPOINT_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-waypoint-ring.png");
    private static final Identifier RETICULE_GREEN_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-reticule_green.png");
    private static final Identifier RETICULE_RED_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-reticule_red.png");
    private static final Identifier BACKGROUND_RED_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-background_red.png");
    private static final Identifier BACKGROUND_GREEN_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-background_green.png");
    private static final Identifier NIGHT_TEXTURE = new Identifier(CompassMod.MODID + ":" + "textures/gui/hud-night.png");

    private static final int MOON_RISE = 12600;
    private static final int SUN_RISE = 22900;
    private static final int MAX_TICKS = 24000;
    private static final double TICKS_TO_SECONDS = 3.6;

    // colors, ARGB
    private static final int COLOR_RED = 0xffff0000;
    private static final int COLOR_GREEN = 0xff00ff00;
    private static final float BK_ALPHA = 0.65f;

    private static final double MSL = 63.0;

    public static boolean isVisible = false;
    private static boolean realTime = false;
    private static boolean altitudeMSL = false;
    private static boolean coordDisplay = false;

    private static int count = 0;

    private static final int HUD_WIDTH = 256;
    private static final int HUD_HEIGHT = 256;
    private static final int Y_PADDING = 20;
    private static final int X_PADDING = 20;
    private static final int BOTTOM = (HUD_HEIGHT + Y_PADDING) / 2;

    private static long lastDebounceTime = 0;
    private static final long DEBOUNCE_DELAY = 250; // milliseconds

    private String alt;
    private String compass;
    private String dist;
    private MinecraftClient client;
    private MatrixStack matrixStack;

    @SuppressWarnings("unused")
    @Override
    public void onHudRender(DrawContext ctx, float tickDelta) {
        if (!isVisible) {
            return;
        }
        client = MinecraftClient.getInstance();
        if (client == null || !client.options.getFullscreen().getValue() || client.getDebugHud().shouldShowDebugHud()) {
            return;
        }
        ClientPlayerEntity player = client.player;
        if (player == null || player.isSleeping() || player.isDead()) {
            return;
        }
        matrixStack = ctx.getMatrices();
        TextRenderer fontRenderer = client.textRenderer;

        int width = client.getWindow().getWidth(); // 1920, scaled is 480
        int hudX = (width - HUD_WIDTH) - X_PADDING;

        int textX = hudX + (HUD_WIDTH / 2);
        int textY = Y_PADDING + (HUD_HEIGHT / 2);

        float curYaw = 0.0f;

        if (player.hasVehicle()) {
            Entity vehicle = player.getVehicle();
            if (vehicle != null) {
                curYaw = vehicle.getYaw();
            }
        } else {
            curYaw = player.getYaw();
        }

        BlockPos currentPos = BlockPos.ofFloored(player.getX(), (player.getY() - (player.getHeight() / 2)), player.getZ());
        double altitude;
        if (altitudeMSL) { // toggle with keybinding, default ',' (comma)
            altitude = getAltitudeMSL(currentPos);
        } else {
            altitude = getCurrentAltitude(currentPos);
        }

        double homeDir = getWaypointDirection(curYaw, CompassMod.wayPointSet);
        double distance = getWaypointDistance(currentPos, CompassMod.wayPointSet);
        double compassHeading = calcCompassHeading(curYaw);

        matrixStack.push();

        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();

        matrixStack.scale(0.25f, 0.25f, 0.25f);

        // draw the background, change color at night
        int textColor;
        Identifier reticuleTexture;
        Identifier backgroundTexture;
        if (client.world != null && isNightTime()) {
            drawTextureFixed(ctx, NIGHT_TEXTURE, hudX);
            textColor = COLOR_RED;
            reticuleTexture = RETICULE_RED_TEXTURE;
            backgroundTexture = BACKGROUND_RED_TEXTURE;
        } else {
            textColor = COLOR_GREEN;
            reticuleTexture = RETICULE_GREEN_TEXTURE;
            backgroundTexture = BACKGROUND_GREEN_TEXTURE;
        }
        drawTextureFixed(ctx, backgroundTexture, hudX);

        // draw the compass ring
        drawTextureWithRotation(ctx, (float) -compassHeading, COMPASS_TEXTURE, hudX);

        // draw the home direction ring
        drawTextureWithRotation(ctx, (float) homeDir, WAYPOINT_TEXTURE, hudX);

        // draw the reticule
        drawTextureFixed(ctx, reticuleTexture, hudX);

        // dampen the updates (20 ticks/second modulo 10 is about 1/2 second updates)
        if (count % 10 == 0) {
            alt = formatBoldFloat(altitude);
            compass = formatBoldFloat(compassHeading);
            dist = formatBoldFloat(distance);
        }
        count++;

        String timeStr = "";
        if (realTime) {
            timeStr = formatBoldStr(formatRealTime());
        } else {
            if (client.world != null) {
                timeStr = formatBoldStr(formatMinecraftTime(client.world.getTimeOfDay()));
            }
        }
        // scale text up to 50%
        matrixStack.scale(2.0f, 2.0f, 2.0f);
        // scale the text coordinates as well
        textX /= 2;
        textY /= 2;

        int hFont = fontRenderer.fontHeight;
        // draw the compass heading text
        ctx.drawText(fontRenderer, compass, textX - fontRenderer.getWidth(compass) / 2, textY - (hFont * 2) - 2, textColor, true);

        // draw the altitude text
        ctx.drawText(fontRenderer, alt, textX - fontRenderer.getWidth(alt) / 2, textY - hFont, textColor, true);

        // draw the distance to the home/spawn point text
        ctx.drawText(fontRenderer, dist, textX - fontRenderer.getWidth(dist) / 2, textY + 2, textColor, true);

        // draw time
        ctx.drawText(fontRenderer, timeStr, textX - fontRenderer.getWidth(timeStr) / 2, textY + hFont + 4, textColor, true);

        // draw the x/y/z position with background
        if (coordDisplay) {
            String positionStr = String.format("§l% 5.1f/% 5.1f/% 5.1f", player.getX(), player.getY(), player.getZ());
            int x1 = (textX - fontRenderer.getWidth(positionStr) / 2) - 2;
            int x2 = (textX + fontRenderer.getWidth(positionStr) / 2) + 2;
            int y = BOTTOM + 5;
            int alpha = (int) (255.0 * BK_ALPHA);
            ctx.fill(x1, y, x2, y + hFont, alpha << 24);
            ctx.drawText(fontRenderer, positionStr, textX - fontRenderer.getWidth(positionStr) / 2, y, textColor, true);
        }

        RenderSystem.disableBlend();

        matrixStack.pop();
    }

    private boolean isNightTime() {
        long ticks = (client.world != null ? client.world.getTimeOfDay() : 0) % MAX_TICKS;
        return ticks > MOON_RISE && ticks < SUN_RISE;
    }

    // draw a fixed texture
    private void drawTextureFixed(@NotNull DrawContext ctx, Identifier texture, int screenX) {
        matrixStack.push();

        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
        ctx.drawTexture(texture, screenX, Y_PADDING, 0, 0, HUD_WIDTH, HUD_HEIGHT);

        matrixStack.pop();
    }

    // draw a rotating texture
    private void drawTextureWithRotation(@NotNull DrawContext ctx, float degrees, Identifier texture, int screenX) {
        matrixStack.push();

        float tx = screenX + (HUD_WIDTH / 2.0f);
        float ty = Y_PADDING + (HUD_HEIGHT / 2.0f);
        // translate to center and rotate
        matrixStack.translate(tx, ty, 0);
        matrixStack.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(degrees));
        matrixStack.translate(-tx, -ty, 0);

        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
        ctx.drawTexture(texture, screenX, Y_PADDING, 0, 0, HUD_WIDTH, HUD_HEIGHT);

        matrixStack.pop();
    }

    // Minecraft font style codes
    // §k	Obfuscated
    // §l	Bold
    // §m	Strikethrough
    // §n	Underline
    // §o	Italic
    // §r	Reset
    private String formatBoldFloat(double d) {
        return String.format("§l%.1f", d);
    }

    private String formatBoldStr(String s) {
        return String.format("§l%s", s);
    }

    private double calcCompassHeading(double yaw) {
        return (((yaw + 180.0) % 360) + 360) % 360;
    }

    // difference angle in degrees the player is facing from the waypoint.
    // zero degrees means the player is facing the waypoint.
    // the waypoint is set with the waypoint command, the waypoint command
    // without arguments resets to the home/spawn point
    private double getWaypointDirection(double yaw, boolean waypoint) {
        BlockPos blockpos;
        if (waypoint) {
            blockpos = new BlockPos(MathHelper.floor(CompassMod.wayPoint.x), 0, MathHelper.floor(CompassMod.wayPoint.y));
        } else {
            blockpos = client.world != null ? client.world.getSpawnPos() : new BlockPos(0, 0, 0);
        }
        Vec3d v = client.player != null ? client.player.getPos() : new Vec3d(0.0, 0.0, 0.0);
        double delta = Math.atan2(blockpos.getZ() - v.z, blockpos.getX() - v.x);
        double relAngle = delta - Math.toRadians(yaw);
        return MathHelper.wrapDegrees(Math.toDegrees(relAngle) - 90.0); // degrees
    }

    // Thanks to Pythagoras we can calculate the distance to home/spawn
    private double getWaypointDistance(BlockPos entityPos, boolean waypoint) {
        BlockPos blockpos;
        if (waypoint) {
            blockpos = new BlockPos(MathHelper.floor(CompassMod.wayPoint.x), 0, MathHelper.floor(CompassMod.wayPoint.y));
        } else {
            blockpos = client.world != null ? client.world.getSpawnPos() : new BlockPos(0, 0, 0);
        }
        double a = Math.pow(blockpos.getZ() - entityPos.getZ(), 2);
        double b = Math.pow(blockpos.getX() - entityPos.getX(), 2);
        return Math.sqrt(a + b);
    }

    // calculate altitude in meters above ground. starting at the entity
    // count down until a non-air block is encountered.
    // only allow altitude calculations in the surface world
    // return a weirdly random number if in nether or end.
    private double getCurrentAltitude(BlockPos entityPos) {
        if (client.world != null && client.world.getRegistryKey().getValue().equals(World.OVERWORLD.getValue())) {
            BlockPos blockPos = new BlockPos(entityPos.getX(), entityPos.getY(), entityPos.getZ());
            while (client.world.isAir(blockPos)) {
                blockPos = blockPos.down();
            }
            // calculate the entity's current altitude above the ground
            return entityPos.getY() - blockPos.getY();
        }
        return 1000.0 * (client.world != null ? client.world.random.nextGaussian() : 0);
    }

    // calculate the altitude above Mean Sea Level (63)
    // this method produces negative number below the sea level
    private double getAltitudeMSL(@NotNull BlockPos bp) {
        return bp.getY() - MSL;
    }

    public String formatMinecraftTime(double ticks) {
        double realSeconds = (ticks % MAX_TICKS) * TICKS_TO_SECONDS; // ticks to seconds wrapping ticks if necessary
        int McHours = MathHelper.floor(((realSeconds / 3600.0) + 6) % 24); // Minecraft's ticks are offset from calendar clock by 6 hours
        int McMinutes = MathHelper.floor((realSeconds / 60.0) % 60);
        int McSeconds = MathHelper.floor(realSeconds % 60);
        return String.format("%02d:%02d:%02d", McHours, McMinutes, McSeconds);
    }

    public String formatRealTime() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return String.format("%s", sdf.format(date));
    }

    public static boolean getIsVisible() {
        return isVisible;
    }

    public static void setIsVisible(boolean value) {
        isVisible = value;
    }

    // toggles HUD visibility using a user defined key, '.' (period) is default
    public static void toggleHUDVisibility() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            isVisible = !isVisible;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

    public static void toggleRealTime() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            realTime = !realTime;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

    public static void toggleAltitude() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            altitudeMSL = !altitudeMSL;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

    public static void toggleCoordDisplay() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            coordDisplay = !coordDisplay;
            lastDebounceTime = System.currentTimeMillis();
        }
    }
}
