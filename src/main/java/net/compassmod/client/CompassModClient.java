/*
 * compassmodClient.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.compassmod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.client.option.KeyBinding;
import org.lwjgl.glfw.GLFW;


@SuppressWarnings("unused")
@Environment(EnvType.CLIENT)
public class CompassModClient implements ClientModInitializer {
    private static final String CATEGORY = "key.category.compassmod.general";
    private static KeyBinding TOGGLE_LOCALTIME;
    private static KeyBinding TOGGLE_VISIBILITY;
    private static KeyBinding TOGGLE_ALTITUDE;
    private static KeyBinding TOGGLE_COORDS;

    @Override
    public void onInitializeClient() {
        System.out.println("Client side Mod Initializer");
        TOGGLE_LOCALTIME = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.compassmod.toggle_real_time", GLFW.GLFW_KEY_R, CATEGORY));
        TOGGLE_VISIBILITY = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.compassmod.toggle_hud", GLFW.GLFW_KEY_PERIOD, CATEGORY));
        TOGGLE_ALTITUDE = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.compassmod.toggle_alt", GLFW.GLFW_KEY_COMMA, CATEGORY));
        TOGGLE_COORDS = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.compassmod.toggle_coords", GLFW.GLFW_KEY_APOSTROPHE, CATEGORY));

        HudRenderCallback.EVENT.register(new CompassRenderer());

        getClientTickHandler().onClientInitialize();

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_VISIBILITY.isPressed()) {
                CompassRenderer.toggleHUDVisibility();
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_LOCALTIME.isPressed()) {
                CompassRenderer.toggleRealTime();
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_ALTITUDE.isPressed()) {
                CompassRenderer.toggleAltitude();
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_COORDS.isPressed()) {
                CompassRenderer.toggleCoordDisplay();
            }
        });
    }

    public static MCClientTickHandler getClientTickHandler() {
        return clientTickHandler;
    }
}
