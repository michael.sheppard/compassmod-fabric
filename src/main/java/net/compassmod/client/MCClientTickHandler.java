package net.mcclockmod.client;

import net.mcclockmod.common.EvictingQueue;
import net.mcclockmod.mixin.MCMinecraftClientAccessor;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;

import java.util.Queue;

public class MCClientTickHandler {
    private final Queue<Integer> averageFps = new EvictingQueue<>(200);

    public void onClientInitialize() {
        ClientTickEvents.START_CLIENT_TICK.register(minecraftClient -> {
            int currentFPS = ((MCMinecraftClientAccessor) minecraftClient).getCurrentFPS();
            averageFps.add(currentFPS);
        });
    }

    public int getAverageFps() {
        int actualAverageFPS = 0;
        for (int fps : averageFps) {
            actualAverageFPS += fps;
        }
        return actualAverageFPS / averageFps.size();
    }

    public int getLowestFps() {
        int temp = -1;
        for (int fps : averageFps) {
            if (temp == -1 || fps < temp) {
                temp = fps;
            }
        }
        return temp;
    }

    public int getHighestFps() {
        int temp = -1;
        for (int fps : averageFps) {
            if (temp == -1 || fps > temp) {
                temp = fps;
            }
        }
        return temp;
    }
}
