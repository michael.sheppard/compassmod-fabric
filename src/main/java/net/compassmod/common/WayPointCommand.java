package net.compassmod.common;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.command.argument.TextArgumentType;
import net.minecraft.command.argument.Vec2ArgumentType;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec2f;


public class WayPointCommand {

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(CommandManager.literal("waypoint").requires((source) -> source.hasPermissionLevel(2))
                .then(CommandManager.argument("reset", TextArgumentType.text())).executes((context) ->
                        executeReset(context.getSource()))
                .then(CommandManager.argument("location", Vec2ArgumentType.vec2()).executes((context) ->
                        executeSet(context.getSource(), Vec2ArgumentType.getVec2(context, "location")))));
    }

    private static int executeSet(ServerCommandSource source, Vec2f location) { // sets new waypoint
        if (location != null) {
            CompassMod.wayPoint = location;
            CompassMod.wayPointSet = true;
            source.sendFeedback(() -> Text.translatable("commands.waypoint.set", CompassMod.wayPoint.x, CompassMod.wayPoint.y), true);
        }
        return 1;
    }

    private static int executeReset(ServerCommandSource source) { // removes waypoint, CompassRenderer will use home/spawn point
        source.sendFeedback(() -> Text.translatable("commands.waypoint.reset"), true);
        CompassMod.wayPointSet = false;
        CompassMod.wayPoint = null;
        return 1;
    }
}
