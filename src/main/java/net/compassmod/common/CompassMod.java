/*
 * CompassMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.compassmod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.minecraft.util.math.Vec2f;

@SuppressWarnings("unused")
public class CompassMod implements ModInitializer {
    static public final String MODID = "compassmod";
    public static Vec2f wayPoint;
    public static boolean wayPointSet;

    @Override
    public void onInitialize() {
        System.out.println("Common side Mod Initializer");
        wayPoint = null;
        wayPointSet = false;
        CommandRegistrationCallback.EVENT.register((dispatcher, registryAccess, environment) -> WayPointCommand.register(dispatcher));
    }

}
