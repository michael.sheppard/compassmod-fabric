package net.compassmod.mixin;

import net.compassmod.client.CompassRenderer;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayerEntity.class)
public abstract class PlayerDeathMixin {

    @Inject(at = @At(value = "HEAD"), method = "onDeath")
    private void killCompassHUD(DamageSource source, CallbackInfo info) {
        if (CompassRenderer.getIsVisible()) {
            CompassRenderer.setIsVisible(false);
        }
    }
}
